package com.example.demo;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static com.example.demo.BrokerConfig.EXCHANGE_NAME;

@SpringBootApplication
public class DemoApplication {
    static List<String> PROCESSED_MESSAGES = new ArrayList<>();

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}


@Component
class Starter implements CommandLineRunner {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Sending message");
        rabbitTemplate.convertAndSend(EXCHANGE_NAME, "x.y.z", "app-message");
    }
}

@Configuration
class BrokerConfig {
    static final String EXCHANGE_NAME = "spring-boot-exchange";

    @Bean
    @Qualifier("queue-foo-bar")
    Queue queueFooBar() {
        return new Queue("spring-boot-foo-bar", false);
    }

    @Bean
    @Qualifier("queue-x-y-z")
    Queue queueXYZ() {
        return new Queue("spring-boot-x-y-z", false);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(EXCHANGE_NAME);
    }

    @Bean
    Binding bindingFooBar(@Qualifier("queue-foo-bar") Queue queueFooBar, TopicExchange exchange) {
        return BindingBuilder.bind(queueFooBar).to(exchange).with("foo.bar");
    }

    @Bean
    Binding bindingXYZ(@Qualifier("queue-x-y-z") Queue queueXYZ, TopicExchange exchange) {
        return BindingBuilder.bind(queueXYZ).to(exchange).with("x.y.z");
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames("spring-boot-foo-bar");
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(Receiver receiver) {
        return new MessageListenerAdapter(receiver, "receiveMessage");
    }
}

@Component
class Receiver {
    public void receiveMessage(String message) {
        DemoApplication.PROCESSED_MESSAGES.add(message);
        System.out.println("Received <" + message + ">");
    }
}