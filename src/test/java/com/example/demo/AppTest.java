package com.example.demo;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.springframework.amqp.rabbit.core.RabbitOperations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static com.example.demo.BrokerConfig.EXCHANGE_NAME;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:test.properties")
public class AppTest {

    private static EmbeddedAMQPBroker broker = new EmbeddedAMQPBroker();

    @Autowired
    private RabbitOperations rabbit;

    @BeforeClass
    public static void startup() throws Exception {
        broker.start();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        broker.stop();
    }

    @Test
    public void handlesFooBarMessages() throws InterruptedException {
        rabbit.convertAndSend(EXCHANGE_NAME, "foo.bar", "test-message");
        Thread.sleep(100);
        assertEquals("test-message", DemoApplication.PROCESSED_MESSAGES.get(0));
    }


    @Test
    public void sendsXYZMessageAtStartup() throws InterruptedException {
        String result = (String)rabbit.receiveAndConvert("spring-boot-x-y-z", 4500);
        assertEquals("app-message", result);
    }

}
