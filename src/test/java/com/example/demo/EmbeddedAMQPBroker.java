package com.example.demo;

import org.apache.qpid.server.SystemLauncher;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class EmbeddedAMQPBroker {
    private static final String INITIAL_CONFIGURATION = "test-broker-config.json";
    private SystemLauncher systemLauncher;

    public EmbeddedAMQPBroker() {
        this.systemLauncher = new SystemLauncher();
    }

    public void start() throws Exception {
        systemLauncher.startup(createSystemConfig());
    }
    public void stop() {
        systemLauncher.shutdown();
    }

    private Map<String, Object> createSystemConfig() {
        Map<String, Object> attributes = new HashMap<>();
        URL initialConfig = EmbeddedAMQPBroker.class.getClassLoader().getResource(INITIAL_CONFIGURATION);
        attributes.put("type", "Memory");
        System.out.println("QPID condig location: " + initialConfig.toExternalForm());
        attributes.put("initialConfigurationLocation", initialConfig.toExternalForm());
        attributes.put("startupLoggedToSystemOut", true);
        return attributes;
    }
}